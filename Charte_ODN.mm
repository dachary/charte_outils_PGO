<map version="freeplane 1.3.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="Charte &#xe9;thique" ID="ID_1723255651" CREATED="1283093380553" MODIFIED="1465658739904"><hook NAME="MapStyle" zoom="0.754">
    <properties show_note_icons="true"/>

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node">
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right">
<stylenode LOCALIZED_TEXT="default" MAX_WIDTH="600" COLOR="#000000" STYLE="as_parent">
<font NAME="SansSerif" SIZE="10" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.note"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right">
<stylenode LOCALIZED_TEXT="styles.topic" COLOR="#18898b" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subtopic" COLOR="#cc3300" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subsubtopic" COLOR="#669900">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.important">
<icon BUILTIN="yes"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000">
<font SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff">
<font SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439">
<font SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000">
<font SIZE="12"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111">
<font SIZE="10"/>
</stylenode>
</stylenode>
</stylenode>
</map_styles>
</hook>
<hook NAME="AutomaticEdgeColor" COUNTER="11"/>
<hook NAME="AlwaysUnfoldedNode"/>
<font SIZE="13"/>
<node TEXT="1 - Existence de la charte" POSITION="right" ID="ID_790843156" CREATED="1460798174081" MODIFIED="1465640727859" STYLE="bubble" VSHIFT="-1">
<font SIZE="16"/>
<edge COLOR="#ff0000"/>
<richcontent TYPE="NOTE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Une charte existe (et d&#233;finition des grands principes)
    </p>
  </body>
</html>

</richcontent>
<node TEXT="D&#xe9;marche citoyenne &amp; globale" ID="ID_1507891523" CREATED="1465566284408" MODIFIED="1465566307847">
<node TEXT="==&gt; notion de coh&#xe9;rence, support, bienveillance, &#xe9;nergie partag&#xe9;e" ID="ID_336393954" CREATED="1465637664591" MODIFIED="1465637667117"/>
<node TEXT="notion aussi de groupe" ID="ID_1862531895" CREATED="1465637669283" MODIFIED="1465637677136"/>
</node>
<node TEXT="Notion d&apos;exp&#xe9;rience &#xe0; valoriser" ID="ID_771451249" CREATED="1465638007350" MODIFIED="1465638023038">
<node TEXT="premier jet d&apos;une charte qui &#xe9;voluera" ID="ID_643060346" CREATED="1465638024980" MODIFIED="1465638030161"/>
<node TEXT="&#xe9;ducation populaire" ID="ID_1812547853" CREATED="1465638030371" MODIFIED="1465638036469"/>
</node>
<node TEXT="Engagements des acteurs" ID="ID_35752444" CREATED="1465565355146" MODIFIED="1465565369875">
<node TEXT="Engagements du commanditaire (celui qui consulte) / du ma&#xee;tre d&apos;ouvrage" ID="ID_1541354443" CREATED="1460800707546" MODIFIED="1465554315074">
<icon BUILTIN="female1"/>
<hook NAME="AlwaysUnfoldedNode"/>
</node>
<node TEXT="Engagements des intervenants techniques (mis &#xe0; disposition de la plate-forme, animation, etc.)" ID="ID_1678664879" CREATED="1460800900457" MODIFIED="1465554276392">
<icon BUILTIN="female2"/>
<hook NAME="AlwaysUnfoldedNode"/>
<node TEXT="AMO" ID="ID_435675967" CREATED="1465554277745" MODIFIED="1465554279463"/>
<node TEXT="Ma&#xee;tres d&apos;oeuvre" ID="ID_41549520" CREATED="1465554280132" MODIFIED="1465554289863"/>
<node TEXT="Ma&#xee;trise d&apos;usage" ID="ID_1165991661" CREATED="1465554290680" MODIFIED="1465554300493"/>
</node>
<node TEXT="Engagements du participant / soci&#xe9;t&#xe9; civile" ID="ID_100769555" CREATED="1460800844460" MODIFIED="1464388217079">
<icon BUILTIN="male1"/>
<hook NAME="AlwaysUnfoldedNode"/>
</node>
</node>
<node TEXT="Obligation de loyaut&#xe9;" ID="ID_1410785512" CREATED="1465565379701" MODIFIED="1465565480659">
<node TEXT="la consultation n&apos;est pas un simulacre / absence de manipulation" ID="ID_1669608614" CREATED="1465567110381" MODIFIED="1465567211654"/>
<node TEXT="organis&#xe9; par une entit&#xe9; determin&#xe9;e pour son compte ou pour un tiers bien d&#xe9;termin&#xe9;" ID="ID_1632764929" CREATED="1465567157219" MODIFIED="1465567200101"/>
<node TEXT="ne pas checher &#xe0; fausser la d&#xe9;marche dans sa repr&#xe9;sentativit&#xe9; (cr&#xe9;ation de comptes fictifs, influence d&apos;opinion, etc.)" ID="ID_1229287714" CREATED="1465567231029" MODIFIED="1465567500936"/>
</node>
<node TEXT="D&#xe9;finie" ID="ID_957746641" CREATED="1460798348711" MODIFIED="1465566063296">
<icon BUILTIN="female1"/>
<icon BUILTIN="full-3"/>
<hook NAME="AlwaysUnfoldedNode"/>
<font SIZE="10"/>
<node TEXT="Date pr&#xe9;cis&#xe9;e" ID="ID_1158068967" CREATED="1465551512413" MODIFIED="1465551518398"/>
<node TEXT="Objet de la consultation dans le cadre" ID="ID_1823553032" CREATED="1465551521612" MODIFIED="1465554398581">
<node TEXT="la consultation couvre t elle enti&#xe8;rement l&apos;objet associ&#xe9;" ID="ID_965924950" CREATED="1465554465473" MODIFIED="1465638895671"/>
<node TEXT="la consultation n&apos;est que l&apos;une des voies utilis&#xe9;es pour r&#xe9;pondre aux enjeux" ID="ID_1714442667" CREATED="1465638895966" MODIFIED="1465638909556"/>
</node>
</node>
<node TEXT="Principe d&apos;une participation ouverte" ID="ID_1429618689" CREATED="1465639651444" MODIFIED="1465653972018"/>
</node>
<node TEXT="2 - Collaboratif / coconstruction" POSITION="right" ID="ID_1964473173" CREATED="1465565501065" MODIFIED="1465640821411">
<edge COLOR="#00ffff"/>
<richcontent TYPE="NOTE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Implication de diff&#233;rentes composantes de l'&#233;tat / de la soci&#233;t&#233; civile dans la d&#233;marche
    </p>
    <p>
      =&gt; sa coconstruction
    </p>
  </body>
</html>

</richcontent>
<font SIZE="16"/>
<node TEXT="La d&#xe9;marche Open Gov implique plusieurs acteurs" ID="ID_247748463" CREATED="1465567732830" MODIFIED="1465638639347">
<node TEXT="acteurs du meme type" ID="ID_347240016" CREATED="1465638659537" MODIFIED="1465638665419"/>
<node TEXT="de plusieurs acteurs de type diff&#xe9;rent (gouvernement / CNN), etc." ID="ID_1541244349" CREATED="1465568776235" MODIFIED="1465638658928"/>
<node TEXT="implication de la soci&#xe9;t&#xe9; civile" ID="ID_62139140" CREATED="1465568800387" MODIFIED="1465568805274">
<node TEXT="" ID="ID_18340609" CREATED="1465640129427" MODIFIED="1465640129427"/>
</node>
</node>
<node TEXT="Forme" ID="ID_1106703182" CREATED="1465568805475" MODIFIED="1465568808324">
<node TEXT="l&apos;implication se fait au stade de la conception de la consultation" ID="ID_737910090" CREATED="1465638678619" MODIFIED="1465638694447"/>
<node TEXT="la collaboration se fait dans le pilotage" ID="ID_1345341232" CREATED="1465638694692" MODIFIED="1465638707195"/>
<node TEXT="la collaboration se fait dans l&apos;examen de la consultation" ID="ID_1485096927" CREATED="1465638707393" MODIFIED="1465638720114"/>
<node TEXT="la collaboration se fait dans tout le suivi du projet normatif" ID="ID_602563026" CREATED="1465638720343" MODIFIED="1465638734756"/>
</node>
<node TEXT="gouvernance" ID="ID_1931301104" CREATED="1465640278631" MODIFIED="1465640280927">
<node TEXT="type" ID="ID_794879674" CREATED="1465640288718" MODIFIED="1465642092569">
<node TEXT="un &#xe9;valuateur distinct de l&apos;organisateur de la consultation" ID="ID_518025054" CREATED="1465655210439" MODIFIED="1465655244080"/>
<node TEXT="comit&#xe9; consultatif" ID="ID_1081313424" CREATED="1465642094560" MODIFIED="1465642095889"/>
<node TEXT="gouvernance de pilotage" ID="ID_1401014726" CREATED="1465642096294" MODIFIED="1465642105010"/>
</node>
<node TEXT="caract&#xe9;ristique" ID="ID_1171746049" CREATED="1465642062450" MODIFIED="1465642067771">
<node TEXT="Claire" ID="ID_689103707" CREATED="1465642051067" MODIFIED="1465642053104"/>
<node TEXT="partagag&#xe9;" ID="ID_1719009326" CREATED="1465640283389" MODIFIED="1465640288030"/>
</node>
</node>
<node TEXT="Stade de la consultation" ID="ID_724448475" CREATED="1465640023853" MODIFIED="1465640030856">
<node TEXT="la consultation se fait d&#xe8;s l&apos;id&#xe9;altion" ID="ID_1080280875" CREATED="1465640033511" MODIFIED="1465640041038"/>
<node TEXT="la consultation se fait au stade de la consultation" ID="ID_169873587" CREATED="1465640041305" MODIFIED="1465640049496"/>
</node>
</node>
<node TEXT="3 - Transparence" POSITION="right" ID="ID_1301679274" CREATED="1465565539365" MODIFIED="1465640454804">
<edge COLOR="#ffff00"/>
<font SIZE="16"/>
<richcontent TYPE="NOTE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Les informations et la d&#233;marches doivent &#234;tre claires, compl&#232;tes, pr&#233;cises et facilement accessibles.
    </p>
  </body>
</html>

</richcontent>
<node TEXT="Auditable" ID="ID_240784720" CREATED="1460798356478" MODIFIED="1465566053473">
<icon BUILTIN="full-2"/>
<icon BUILTIN="video"/>
<hook NAME="AlwaysUnfoldedNode"/>
<node TEXT="&#xe0; quel stade" ID="ID_1412423152" CREATED="1465552418847" MODIFIED="1465552423584">
<node TEXT="apr&#xe8;s" ID="ID_1586277889" CREATED="1460798907252" MODIFIED="1464388217088">
<icon BUILTIN="female1"/>
<hook NAME="AlwaysUnfoldedNode"/>
</node>
<node TEXT="pendant" ID="ID_1338778884" CREATED="1460798900850" MODIFIED="1464388217087">
<icon BUILTIN="female2"/>
<hook NAME="AlwaysUnfoldedNode"/>
<node TEXT="d&#xe9;tection d&apos;anomalies par rapport &#xe0; la consultation (violation d&apos;une proc&#xe9;dure / r&#xe8;gle)" ID="ID_1731667074" CREATED="1465553566407" MODIFIED="1465566069817">
<node TEXT="Avoir un r&#xe9;f&#xe9;rent en cas de pb : voir les personnes en place" ID="ID_175283948" CREATED="1465552003253" MODIFIED="1465554816261"/>
<node TEXT="page d&#xe9;di&#xe9;e publique" ID="ID_1057964794" CREATED="1465554792540" MODIFIED="1465554798588"/>
<node TEXT="sanction : permet de supprimer les contributions" ID="ID_823131049" CREATED="1465555762849" MODIFIED="1465555773015"/>
</node>
</node>
<node TEXT="avant" ID="ID_536167063" CREATED="1460801219763" MODIFIED="1464388217085">
<icon BUILTIN="female1"/>
<hook NAME="AlwaysUnfoldedNode"/>
<node TEXT="dans la commande publique ? / cahier des charges" ID="ID_836548247" CREATED="1465552756129" MODIFIED="1465552911338"/>
<node TEXT="par la pr&#xe9;sence de personnes tierces" ID="ID_1113342667" CREATED="1465552764370" MODIFIED="1465552804086"/>
</node>
</node>
<node TEXT="par qui" ID="ID_535193726" CREATED="1465552431454" MODIFIED="1465552435105">
<node TEXT="par le commanditaire lui-m&#xea;me" ID="ID_1210583467" CREATED="1465552437237" MODIFIED="1465552456046">
<node TEXT="directement" ID="ID_983748291" CREATED="1465552469194" MODIFIED="1465552471445"/>
<node TEXT="indirectement" ID="ID_1036514767" CREATED="1465552471688" MODIFIED="1465552474066"/>
</node>
<node TEXT="par plusieurs organisations" ID="ID_1593452351" CREATED="1465552540043" MODIFIED="1465552546766"/>
<node TEXT="par ONG" ID="ID_1670750681" CREATED="1465552479959" MODIFIED="1465552554543"/>
</node>
<node TEXT="comment" ID="ID_1740416553" CREATED="1465635010770" MODIFIED="1465635012525"/>
</node>
<node TEXT="Modalit&#xe9;s op&#xe9;rationnelle de mise en oeuvre" ID="ID_181072707" CREATED="1460802237207" MODIFIED="1465655636209" STYLE="bubble" HGAP="55" VSHIFT="12">
<hook NAME="AlwaysUnfoldedNode"/>
<node TEXT="Localis&#xe9; dans le territoire" ID="ID_736901973" CREATED="1460802316302" MODIFIED="1464388217090">
<icon BUILTIN="female2"/>
<hook NAME="AlwaysUnfoldedNode"/>
<node TEXT="h&#xe9;bergement sur le territoire" ID="ID_225115997" CREATED="1465555302534" MODIFIED="1465555311233"/>
<node TEXT="Dans les langues du territoire" ID="ID_1204319955" CREATED="1460803167639" MODIFIED="1465551546899" VSHIFT="24">
<icon BUILTIN="female1"/>
<icon BUILTIN="female2"/>
<hook NAME="AlwaysUnfoldedNode"/>
</node>
</node>
<node TEXT="Par Logiciels &amp; algo ouverts" ID="ID_627513535" CREATED="1460798294614" MODIFIED="1465555352280">
<hook NAME="AlwaysUnfoldedNode"/>
<node TEXT="Accessible" ID="ID_792313026" CREATED="1460798370469" MODIFIED="1464388217103">
<icon BUILTIN="female2"/>
<hook NAME="AlwaysUnfoldedNode"/>
</node>
<node TEXT="Ouvert" ID="ID_914821653" CREATED="1460798376803" MODIFIED="1464388217103">
<icon BUILTIN="female2"/>
<icon BUILTIN="penguin"/>
<hook NAME="AlwaysUnfoldedNode"/>
</node>
<node TEXT="Auditable" ID="ID_10258568" CREATED="1460798378457" MODIFIED="1464388217104">
<icon BUILTIN="male1"/>
<icon BUILTIN="female1"/>
<hook NAME="AlwaysUnfoldedNode"/>
</node>
<node TEXT="interop&#xe9;rabilit&#xe9;" ID="ID_1109110499" CREATED="1460801004129" MODIFIED="1464388217105">
<icon BUILTIN="female2"/>
<icon BUILTIN="female1"/>
<hook NAME="AlwaysUnfoldedNode"/>
</node>
</node>
<node TEXT="transfert de donn&#xe9;es aux services ext&#xe9;rieurs" ID="ID_124304510" CREATED="1460802325376" MODIFIED="1464388217092">
<hook NAME="AlwaysUnfoldedNode"/>
<node TEXT="inexistants" ID="ID_149888739" CREATED="1460802537077" MODIFIED="1464388217106">
<icon BUILTIN="female2"/>
<icon BUILTIN="full-1"/>
<hook NAME="AlwaysUnfoldedNode"/>
</node>
<node TEXT="identifi&#xe9;s" ID="ID_469790425" CREATED="1460802541108" MODIFIED="1464388217107">
<icon BUILTIN="female2"/>
<icon BUILTIN="full-2"/>
<hook NAME="AlwaysUnfoldedNode"/>
<node TEXT="quelles donn&#xe9;es" ID="ID_1673023289" CREATED="1460802553497" MODIFIED="1460802560200"/>
<node TEXT="&#xe0; qui" ID="ID_1095308901" CREATED="1460802560473" MODIFIED="1460802562783"/>
<node TEXT="possible de opti in / out" ID="ID_894806475" CREATED="1465555454731" MODIFIED="1465555461112"/>
</node>
</node>
</node>
<node TEXT="Donn&#xe9;es issues de la consultation" ID="ID_925431254" CREATED="1460798300957" MODIFIED="1465655642147" STYLE="bubble" HGAP="68" VSHIFT="-71">
<hook NAME="AlwaysUnfoldedNode"/>
<node TEXT="Consultables" ID="ID_1556753787" CREATED="1460798403290" MODIFIED="1464388217094">
<icon BUILTIN="female2"/>
<icon BUILTIN="female1"/>
<hook NAME="AlwaysUnfoldedNode"/>
<node TEXT="expliquer lesquelles ?" ID="ID_876324044" CREATED="1465556093543" MODIFIED="1465556101056"/>
</node>
<node TEXT="Exhaustives (logs) dans le respect de la vie priv&#xe9;e" ID="ID_288124726" CREATED="1460800483102" MODIFIED="1464388217094">
<icon BUILTIN="female2"/>
<icon BUILTIN="female1"/>
<hook NAME="AlwaysUnfoldedNode"/>
</node>
<node TEXT="Int&#xe8;gres" ID="ID_270263074" CREATED="1460801724150" MODIFIED="1464388217095">
<icon BUILTIN="female2"/>
<icon BUILTIN="female1"/>
<hook NAME="AlwaysUnfoldedNode"/>
</node>
<node TEXT="Ouvertes" ID="ID_1805425217" CREATED="1460798410295" MODIFIED="1465555939262" HGAP="32" VSHIFT="-13">
<icon BUILTIN="female1"/>
<icon BUILTIN="female2"/>
<hook NAME="AlwaysUnfoldedNode"/>
<node TEXT="Open Data" ID="ID_464985462" CREATED="1460802002947" MODIFIED="1464388217118">
<icon BUILTIN="penguin"/>
<hook NAME="AlwaysUnfoldedNode"/>
<node TEXT="Licence ODbL" ID="ID_1997632866" CREATED="1460802073681" MODIFIED="1460804819067">
<icon BUILTIN="full-1"/>
</node>
<node TEXT="Licence Ouverte" ID="ID_1847498093" CREATED="1460802081281" MODIFIED="1460804828533">
<icon BUILTIN="full-2"/>
</node>
</node>
<node TEXT="Format ouvert" ID="ID_280420001" CREATED="1460801796734" MODIFIED="1464388217128">
<hook NAME="AlwaysUnfoldedNode"/>
</node>
<node TEXT="interop&#xe9;rables" ID="ID_1787294527" CREATED="1460801950515" MODIFIED="1464388217129">
<hook NAME="AlwaysUnfoldedNode"/>
</node>
</node>
<node TEXT="Audit&#xe9;es" ID="ID_1110552007" CREATED="1460798414086" MODIFIED="1464388217097">
<icon BUILTIN="male1"/>
<hook NAME="AlwaysUnfoldedNode"/>
<node TEXT="gr&#xe2;ce &#xe0; des outils fournis" ID="ID_934904138" CREATED="1460802996408" MODIFIED="1464388217129">
<icon BUILTIN="family"/>
<hook NAME="AlwaysUnfoldedNode"/>
<node TEXT="visualisation sur la plate forme" ID="ID_606593380" CREATED="1465556117777" MODIFIED="1465556125524"/>
</node>
<node TEXT="gr&#xe2;ce &#xe0; des outils tiers" ID="ID_1048046287" CREATED="1460820475755" MODIFIED="1464388217130">
<icon BUILTIN="male1"/>
<hook NAME="AlwaysUnfoldedNode"/>
</node>
</node>
</node>
<node TEXT="6 - Influence" ID="ID_1119807380" CREATED="1460800501624" MODIFIED="1465567617644" STYLE="bubble">
<hook NAME="AlwaysUnfoldedNode"/>
<node TEXT="Identifier les Lobbying" ID="ID_827835923" CREATED="1460798310683" MODIFIED="1464388217100">
<icon BUILTIN="female2"/>
<icon BUILTIN="male1"/>
<hook NAME="AlwaysUnfoldedNode"/>
<node TEXT="liste" ID="ID_938061574" CREATED="1460820525116" MODIFIED="1464388217178">
<hook NAME="AlwaysUnfoldedNode"/>
</node>
<node TEXT="identification visuelle" ID="ID_109954977" CREATED="1460820527394" MODIFIED="1464388217178">
<hook NAME="AlwaysUnfoldedNode"/>
</node>
</node>
<node TEXT="Influences sociales" ID="ID_1621667134" CREATED="1460800513963" MODIFIED="1464388217101">
<hook NAME="AlwaysUnfoldedNode"/>
<node TEXT="Alerter / former" ID="ID_621507501" CREATED="1460803837697" MODIFIED="1464388217179">
<icon BUILTIN="female1"/>
<icon BUILTIN="male1"/>
<hook NAME="AlwaysUnfoldedNode"/>
</node>
<node TEXT="Limiter" ID="ID_1661134804" CREATED="1460803840233" MODIFIED="1464388217179">
<icon BUILTIN="female2"/>
<hook NAME="AlwaysUnfoldedNode"/>
</node>
</node>
<node TEXT="Identfication des utilisateurs" ID="ID_1304958621" CREATED="1465555480739" MODIFIED="1465566079126">
<node TEXT="anonyme" ID="ID_316418542" CREATED="1465555494234" MODIFIED="1465555497463"/>
<node TEXT="non anonyme" ID="ID_165155875" CREATED="1465555498170" MODIFIED="1465555500875"/>
</node>
</node>
</node>
<node TEXT="4 - Participatif" POSITION="right" ID="ID_438881569" CREATED="1465565784970" MODIFIED="1465567805245">
<edge COLOR="#7c0000"/>
<richcontent TYPE="NOTE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      quel est le degr&#233; de participation attendu
    </p>
  </body>
</html>
</richcontent>
<font SIZE="16"/>
<node TEXT="Defgr&#xe9; de participation" ID="ID_1415112865" CREATED="1465566112687" MODIFIED="1465566117140">
<node TEXT="Informer" ID="ID_232179014" CREATED="1465566118732" MODIFIED="1465566121378"/>
<node TEXT="Consulter" ID="ID_95943262" CREATED="1465566121640" MODIFIED="1465566124624"/>
<node TEXT="Impliquer" ID="ID_994998117" CREATED="1465566125149" MODIFIED="1465566128067"/>
<node TEXT="Collaborer" ID="ID_1116345047" CREATED="1465566128520" MODIFIED="1465566132803"/>
<node TEXT="Empowering" ID="ID_1136369989" CREATED="1465566133520" MODIFIED="1465566136258"/>
</node>
<node TEXT="5 - Services attendus &amp; m&#xe9;thodes" ID="ID_632619516" CREATED="1461182241633" MODIFIED="1465640216459" VSHIFT="-16">
<hook NAME="AlwaysUnfoldedNode"/>
<font SIZE="10"/>
<node TEXT="Questionnaire" ID="ID_171627147" CREATED="1465556165118" MODIFIED="1465556167823"/>
<node TEXT="Votes" ID="ID_773683560" CREATED="1460798304623" MODIFIED="1464388217098" STYLE="bubble">
<hook NAME="AlwaysUnfoldedNode"/>
<node TEXT="Uniques" ID="ID_1784314880" CREATED="1460801592210" MODIFIED="1464388217130">
<icon BUILTIN="female2"/>
<icon BUILTIN="female1"/>
<hook NAME="AlwaysUnfoldedNode"/>
</node>
<node TEXT="Question de l&apos;Anonymation" ID="ID_505209977" CREATED="1460798426110" MODIFIED="1464388217132">
<hook NAME="AlwaysUnfoldedNode"/>
<node TEXT="Par d&#xe9;faut garantie de l&apos;anonymat" ID="ID_1779997427" CREATED="1460798509901" MODIFIED="1460803495695">
<icon BUILTIN="female2"/>
<icon BUILTIN="female1"/>
</node>
<node TEXT="Possibilit&#xe9; identification (permet le suivi de contributions)" ID="ID_951925000" CREATED="1460803442003" MODIFIED="1461182172015">
<icon BUILTIN="male1"/>
<node TEXT="identit&#xe9; r&#xe9;elle" ID="ID_939879669" CREATED="1460803737373" MODIFIED="1460803769772">
<icon BUILTIN="male1"/>
<icon BUILTIN="female2"/>
</node>
<node TEXT="pseudonyme" ID="ID_1155267552" CREATED="1460803749013" MODIFIED="1460803755984">
<icon BUILTIN="male1"/>
</node>
</node>
</node>
</node>
<node TEXT="Commentaires" ID="ID_179944493" CREATED="1461182272742" MODIFIED="1464388217099">
<icon BUILTIN="female2"/>
<hook NAME="AlwaysUnfoldedNode"/>
<node TEXT="&#xc0; l&apos;&#xe9;chelle" ID="ID_1285717938" CREATED="1461182382073" MODIFIED="1464388217145">
<hook NAME="AlwaysUnfoldedNode"/>
<node TEXT="de l&apos;article" ID="ID_815214877" CREATED="1461182399251" MODIFIED="1461182419987">
<icon BUILTIN="full-2"/>
</node>
<node TEXT="du paragraphe" ID="ID_421570059" CREATED="1461182402426" MODIFIED="1461182416033">
<icon BUILTIN="full-1"/>
</node>
</node>
<node TEXT="Types" ID="ID_1482922643" CREATED="1461182425755" MODIFIED="1464388217156">
<hook NAME="AlwaysUnfoldedNode"/>
<node TEXT="+1 / -1" ID="ID_448122785" CREATED="1461182430884" MODIFIED="1461182507186">
<icon BUILTIN="full-2"/>
</node>
<node TEXT="multicrit&#xe8;res" ID="ID_1582999334" CREATED="1461182482600" MODIFIED="1461182542115">
<icon BUILTIN="full-1"/>
<node TEXT="r&#xe9;daction" ID="ID_1072603694" CREATED="1461182551878" MODIFIED="1461182555400"/>
<node TEXT="formulation" ID="ID_480381340" CREATED="1461182558626" MODIFIED="1461182561368"/>
<node TEXT="finalit&#xe9;" ID="ID_106840756" CREATED="1461182555761" MODIFIED="1461182557975"/>
<node TEXT="etc." ID="ID_1422436266" CREATED="1461182569957" MODIFIED="1461182571189"/>
</node>
</node>
</node>
<node TEXT="Articles alternatifs" ID="ID_288962940" CREATED="1461182288677" MODIFIED="1464388217099">
<hook NAME="AlwaysUnfoldedNode"/>
<node TEXT="texte propos&#xe9;" ID="ID_1078590229" CREATED="1460799922592" MODIFIED="1464388217164">
<icon BUILTIN="female1"/>
<hook NAME="AlwaysUnfoldedNode"/>
<node TEXT="versions" ID="ID_1344156919" CREATED="1460804855366" MODIFIED="1460804865585">
<node TEXT="un texte final" ID="ID_57845508" CREATED="1460804868732" MODIFIED="1460804882858">
<icon BUILTIN="full-2"/>
</node>
<node TEXT="plusieurs variations" ID="ID_571364463" CREATED="1460799936154" MODIFIED="1460804877901">
<icon BUILTIN="full-1"/>
</node>
</node>
<node TEXT="Hierarchisation" ID="ID_895774199" CREATED="1460799941526" MODIFIED="1460804897915">
<node TEXT="en dessous" ID="ID_1193746265" CREATED="1460804905179" MODIFIED="1460804912171">
<icon BUILTIN="full-2"/>
</node>
<node TEXT="m&#xea;me niveau que les propositions" ID="ID_165918718" CREATED="1460804900843" MODIFIED="1460804914994">
<icon BUILTIN="full-1"/>
</node>
</node>
</node>
<node TEXT="r&#xe9;dactions compl&#xe9;mentaires" ID="ID_643199677" CREATED="1460804072179" MODIFIED="1464388217171">
<hook NAME="AlwaysUnfoldedNode"/>
<node TEXT="individuelles" ID="ID_263714346" CREATED="1460804697846" MODIFIED="1460804747772">
<icon BUILTIN="male1"/>
<icon BUILTIN="full-2"/>
</node>
<node TEXT="collectives (via git-like)" ID="ID_1703417193" CREATED="1460804709021" MODIFIED="1460804785432">
<icon BUILTIN="male1"/>
<icon BUILTIN="full-1"/>
</node>
</node>
</node>
</node>
<node TEXT="aspects inclusifs" ID="ID_1572796511" CREATED="1465640243822" MODIFIED="1465640249143">
<node TEXT="ergonomie" ID="ID_1370463706" CREATED="1465554343989" MODIFIED="1465566085143">
<node TEXT="utilisation de pictogrammes pour facilit&#xe9; la compr&#xe9;hesion (d&#xe9;finir notion/concept)" ID="ID_1605335893" CREATED="1465553675451" MODIFIED="1465554031773">
<node TEXT="transparence" ID="ID_131235657" CREATED="1465553922699" MODIFIED="1465553927610"/>
<node TEXT="Participation" ID="ID_398462263" CREATED="1465553928327" MODIFIED="1465553933630"/>
</node>
<node TEXT="accessibilit&#xe9; des informations / questions / texte" ID="ID_645643795" CREATED="1465568405610" MODIFIED="1465568426948"/>
<node TEXT="processus graphique de suivi dans le temps (&#xe9;tape de la l&#xe9;gistique)" ID="ID_1475742107" CREATED="1465551592739" MODIFIED="1465638839029"/>
<node TEXT="explication" ID="ID_824386794" CREATED="1465554535878" MODIFIED="1465554538110">
<node TEXT="vulgarisation" ID="ID_831579391" CREATED="1465554543202" MODIFIED="1465554548476"/>
<node TEXT="positionner dans le cadre de la m&#xe9;thodologique" ID="ID_1259462483" CREATED="1465554548712" MODIFIED="1465554556074"/>
</node>
</node>
<node TEXT="avec une continuit&#xe9; physique / num&#xe9;rique" ID="ID_691767800" CREATED="1465554648351" MODIFIED="1465566328719">
<icon BUILTIN="video"/>
<node TEXT="Assorties d&apos;ateliers physiques en //" ID="ID_1725080378" CREATED="1460800399620" MODIFIED="1465554507659">
<icon BUILTIN="family"/>
<icon BUILTIN="full-1"/>
<hook NAME="AlwaysUnfoldedNode"/>
<node TEXT="&#xe0; creuser" ID="ID_630437037" CREATED="1465554493238" MODIFIED="1465554507658" VSHIFT="-24"/>
</node>
<node TEXT="avec des outils qui permettent de croiser les deux dimensions" ID="ID_187211994" CREATED="1465554691928" MODIFIED="1465554766072">
<node TEXT="not. en r&#xe9;introduisant les outils dans les r&#xe9;unions physiques" ID="ID_96304294" CREATED="1465554766073" MODIFIED="1465554776410"/>
</node>
</node>
<node TEXT="respect de la diversit&#xe9;" ID="ID_1211204944" CREATED="1465654119809" MODIFIED="1465654258570">
<node TEXT="mise en avant d&apos;avis minoritaires / signaux faibles" ID="ID_1392202548" CREATED="1465654142892" MODIFIED="1465656227007"/>
<node TEXT="" ID="ID_933319518" CREATED="1465654174948" MODIFIED="1465654174948"/>
</node>
</node>
</node>
<node TEXT="5 - Continuit&#xe9;" POSITION="right" ID="ID_460405570" CREATED="1465565792741" MODIFIED="1465640580230">
<edge COLOR="#00007c"/>
<richcontent TYPE="NOTE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      jusqu'&#224; o&#249; seront impliqu&#233;s les contributeurs / positionner aussi la consultation dans une globalit&#233;
    </p>
  </body>
</html>

</richcontent>
<font SIZE="16"/>
<node TEXT="valoriser les exp&#xe9;riences pass&#xe9;es, la d&#xe9;marche globale" ID="ID_1884196057" CREATED="1465640528406" MODIFIED="1465640547382">
<node TEXT="id&#xe9;e de r&#xe9;seau maill&#xe9;" ID="ID_829741041" CREATED="1465640585027" MODIFIED="1465640589642"/>
<node TEXT="cette aspect pourrait aussi etre contributive" ID="ID_31916856" CREATED="1465640596652" MODIFIED="1465640608763">
<node TEXT="il faudrait une plate forme/un outil qui donne cette vision globale" ID="ID_1528272068" CREATED="1465640624369" MODIFIED="1465640634847"/>
</node>
</node>
<node TEXT="Contribution limit&#xe9;e &#xe0; la consultation initiale" ID="ID_1257408141" CREATED="1465636829191" MODIFIED="1465636836695"/>
<node TEXT="Contributeurs inform&#xe9;s des suites de la consultation" ID="ID_28960146" CREATED="1465636868046" MODIFIED="1465636906054">
<node TEXT="au fur &amp; &#xe0; mesure du processus legistique" ID="ID_1758612510" CREATED="1465640763941" MODIFIED="1465640778556"/>
</node>
<node TEXT="Contributeurs associ&#xe9;s jusqu&apos;&#xe0; la finalisation de la d&#xe9;marche" ID="ID_1153606214" CREATED="1465636836986" MODIFIED="1465636866631">
<node TEXT="des outils mis &#xe0; dispostion pour continuer leur travail" ID="ID_185042563" CREATED="1465640504237" MODIFIED="1465640514427"/>
<node TEXT="Parlement &#xe9;valuateur avec la soci&#xe9;t&#xe9; civile" ID="ID_341944585" CREATED="1465640779191" MODIFIED="1465655489090"/>
</node>
</node>
<node TEXT="6 - Respect des engagements" POSITION="right" ID="ID_1750863415" CREATED="1465565809865" MODIFIED="1465653420026">
<edge COLOR="#007c00"/>
<richcontent TYPE="NOTE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      accountability : le commanditaire s'oblige &#224; respecter et rendre compte de ses engagements au titre de la plate forme.
    </p>
  </body>
</html>
</richcontent>
<font SIZE="16"/>
<node TEXT="Engagement du commanditaire" ID="ID_547801461" CREATED="1460799279578" MODIFIED="1465650335423">
<icon BUILTIN="female1"/>
<hook NAME="AlwaysUnfoldedNode"/>
<node TEXT="limit&#xe9; aux th&#xe9;matiques sp&#xe9;cifi&#xe9;es en amont" ID="ID_1210167832" CREATED="1465650336749" MODIFIED="1465650339782"/>
<node TEXT="&#xe9;tendu aux th&#xe9;matiques ayant retenu X contributions" ID="ID_275404313" CREATED="1465650305558" MODIFIED="1465650346714"/>
</node>
<node TEXT="Mise en place d&apos;une boite &#xe0; id&#xe9;es?" ID="ID_590181621" CREATED="1460799501173" MODIFIED="1465653420026">
<icon BUILTIN="female1"/>
<hook NAME="AlwaysUnfoldedNode"/>
<node TEXT="proposition d&apos;id&#xe9;es de th&#xe9;matiques" ID="ID_1560596271" CREATED="1460804054897" MODIFIED="1460804067688">
<icon BUILTIN="male1"/>
</node>
</node>
<node TEXT="nomination d&apos;une personne en charge du suivi des engagements" ID="ID_628377856" CREATED="1465640907717" MODIFIED="1465640934980"/>
</node>
</node>
</map>